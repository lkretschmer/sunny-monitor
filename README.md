# Sunny-Monitor

This is a simple monitor application provided as example how to use the
[sunny](https://gitlab.com/bboehmke/sunny/) library. This application 
requires exactly one energy meter, one PV inverter and one battery inverter.

If the application is started it will discover this devices and starts a 
little web server that is reachable under http://127.0.0.1:8080 which
displays the actual state of the devices.

![monitor_app](monitor_app.png)

The following configurations can be changed via environment variables:

| Variable         | Description                                | 
|------------------|--------------------------------------------|
| PASSWORD         | User password for inverter (Default: 0000) | 
| SOLAR_INVERTER   | Address of PV inverter                     | 
| BATTERY_INVERTER | Address of battery inverter                | 
| ENERGY_METER     | Address of energy meter                    | 
| PORT             | Port of the web server                     | 

## Docker

If you want to run the application in a docker container, run these commands:

### Build
```
docker build -t sunny-monitor . 
```

### Run
```
docker run -d --rm \
    -e BATTERY_INVERTER=<IP of battery inverter> \
    -e ENERGY_METER=<IP of energy meter> \
    -e SOLAR_INVERTER=<IP of PV inverter> \
    -e PASSWORD=<user password> \
    --name sunny-monitor \
    --network host sunny-monitor
```