// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"embed"
	"io/fs"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"

	"gitlab.com/bboehmke/sunny"
)

//go:embed assets/*
var assets embed.FS

var websocketUpgrader = websocket.Upgrader{}

// SolarState contains current inverter and energy meter values
type SolarState struct {
	// >0 - Solar production
	SolarPower int32 `json:"solar_power"`
	// >0 - buy energy | <0 sell energy
	SupplierPower int32 `json:"supplier_power"`
	// <0 charge | >0 discharge
	BatteryPower int32 `json:"battery_power"`

	HomePower         uint32 `json:"home_power"`
	HomeSupplierPower uint32 `json:"home_supplier_power"`
	HomeOwnPower      uint32 `json:"home_own_power"`

	BatteryCharge uint32 `json:"battery_charge"`
}

// MonitorService for updating the actual solar state
type MonitorService struct {
	sync.RWMutex

	EnergyMeter     *sunny.Device
	SolarInverter   *sunny.Device
	BatteryInverter *sunny.Device

	state SolarState
}

func (s *MonitorService) getState() SolarState {
	s.RLock()
	defer s.RUnlock()
	return s.state
}

func (s *MonitorService) updateLoop() {
	for {
		// no sleep required -> EnergyMeter values will received once a second
		s.update()
	}
}
func (s *MonitorService) update() {
	energyMeterValues, err := s.EnergyMeter.GetValues()
	if err != nil {
		logrus.Warning(err)
		return
	}
	pvValues, err := s.SolarInverter.GetValues()
	if err != nil {
		logrus.Warning(err)
		return
	}
	batteryValues, err := s.BatteryInverter.GetValues()
	if err != nil {
		logrus.Warning(err)
		return
	}

	// get energy meter values
	supplierPlus, ok := energyMeterValues[sunny.ActivePowerPlus]
	if !ok || supplierPlus == nil {
		supplierPlus = uint64(0)
	}
	supplierMinus, ok := energyMeterValues[sunny.ActivePowerMinus]
	if !ok || supplierMinus == nil {
		supplierMinus = uint64(0)
	}

	// get pv value
	pvPower := pvValues[sunny.ActivePowerPlus]
	if !ok || pvPower == nil {
		pvPower = int32(0)
	}

	// get battery values
	batteryPower := batteryValues[sunny.ActivePowerPlus]
	if !ok || batteryPower == nil {
		batteryPower = int32(0)
	}
	batteryCharge := batteryValues[sunny.BatteryCharge]
	if !ok || batteryCharge == nil {
		batteryCharge = uint32(0)
	}

	state := SolarState{
		SolarPower:    pvPower.(int32),
		SupplierPower: int32(supplierPlus.(float64) - supplierMinus.(float64)),
		BatteryPower:  batteryPower.(int32),
		BatteryCharge: batteryCharge.(uint32),
	}

	usage := state.SolarPower + state.SupplierPower + state.BatteryPower
	if usage < 0 {
		return // ignore this update
	}

	state.HomePower = uint32(usage)
	if state.SupplierPower > 0 {
		state.HomeSupplierPower = uint32(state.SupplierPower)
	}
	state.HomeOwnPower = state.HomePower - state.HomeSupplierPower

	s.Lock()
	defer s.Unlock()
	s.state = state
}

func getEnv(key, def string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return def
}

func getDeviceFromEnv(connection *sunny.Connection, key, password string) (*sunny.Device, error) {
	address, ok := os.LookupEnv(key)
	if !ok {
		return nil, nil
	}

	return connection.NewDevice(address, password)
}

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true,
		DisableColors:   true,
	})
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.DebugLevel)

	// read configuration from environment variables
	if strings.ToLower(os.Getenv("DEBUG")) != "true" {
		gin.SetMode(gin.ReleaseMode)
	}

	// create new connection and set interface if given
	connection, err := sunny.NewConnection(getEnv("INTERFACE", ""))
	if err != nil {
		logrus.Fatal(err)
	}

	password := getEnv("PASSWORD", "0000")

	service := MonitorService{}

	service.SolarInverter, err = getDeviceFromEnv(connection, "SOLAR_INVERTER", password)
	if err != nil {
		logrus.Fatal(err)
	}
	service.BatteryInverter, err = getDeviceFromEnv(connection, "BATTERY_INVERTER", password)
	if err != nil {
		logrus.Fatal(err)
	}
	service.EnergyMeter, err = getDeviceFromEnv(connection, "ENERGY_METER", password)
	if err != nil {
		logrus.Fatal(err)
	}

	devices := connection.SimpleDiscoverDevices(password)

	for _, dev := range devices {
		deviceClass, err := dev.GetValue(sunny.DeviceClass)
		if err != nil {
			logrus.Fatalf("Failed to get device type %v", err)
		}

		var solarClass uint32 = 8001
		var batteryClass uint32 = 8007

		switch deviceClass {
		case 1: // energy meter
			if service.EnergyMeter == nil {
				service.EnergyMeter = dev
			}

		case solarClass: // solar
			if service.SolarInverter == nil {
				service.SolarInverter = dev
			}

		case batteryClass: // battery
			if service.BatteryInverter == nil {
				service.BatteryInverter = dev
			}

		default:
			logrus.Warningf("Unknown device class %d", deviceClass)
		}

	}

	if service.EnergyMeter == nil {
		logrus.Fatal("Energy meter missing")
	}
	if service.SolarInverter == nil {
		logrus.Fatal("Solar inverter missing")
	}
	if service.BatteryInverter == nil {
		logrus.Fatal("Battery inverter missing")
	}

	go service.updateLoop()

	router := gin.New()
	router.Use(gin.Logger(), gin.Recovery())

	assetFs, _ := fs.Sub(assets, "assets")
	handler := fsHandler(http.FS(assetFs))
	router.GET("/", handler)
	router.HEAD("/", handler)
	router.GET("/status_img.svg", handler)
	router.HEAD("/status_img.svg", handler)

	router.GET("/ws", ws(&service))

	err = router.Run(":" + getEnv("PORT", "8080"))
	if err != nil {
		logrus.Fatal(err)
	}
}

func fsHandler(fs http.FileSystem) gin.HandlerFunc {
	fileServer := http.FileServer(fs)
	return func(ctx *gin.Context) {
		fileServer.ServeHTTP(ctx.Writer, ctx.Request)
	}
}

func ws(service *MonitorService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		conn, err := websocketUpgrader.Upgrade(ctx.Writer, ctx.Request, nil)
		if err != nil {
			logrus.Error("Failed to upgrade protocol:", err)
			return
		}

		counter := 1
		for {
			err := conn.WriteJSON(service.getState())
			if err != nil {
				conn.Close()
				break
			}
			time.Sleep(time.Second)
			counter++
		}
	}
}
