module gitlab.com/bboehmke/sunny-monitor

go 1.16

require (
	github.com/gin-gonic/gin v1.9.1
	github.com/gorilla/websocket v1.5.0
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/bboehmke/sunny v0.15.0
)
