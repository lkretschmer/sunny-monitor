# Build the application from source
FROM golang:1.21 AS build-stage

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY *.go ./
COPY assets ./assets

RUN CGO_ENABLED=0 go build -o /sunny-monitor

# Deploy the application binary into a lean image
FROM scratch AS release-stage

WORKDIR /

COPY --from=build-stage /sunny-monitor /sunny-monitor

USER 1000:1000

ENTRYPOINT ["/sunny-monitor"]
